import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

class App extends Component {
  state = {
    venues: [],
    axiosError: false
  };

  componentDidMount() {
    const endpoint = 'https://api.foursquare.com/v2/venues/explore?';
    const config = {
      params: {
        client_id: 'SUA_CHAVE',
        client_secret: 'SEU_SEGREDO',
        v: '20180323',
        ll: '40.7243,-74.0018',
        query: 'coffee'
      }
    };

    axios
      .get(endpoint, config)
      .then(response => {
        const venuesResponse = response.data.response.groups['0'].items; // Pegamos os lugares

        let venuesArray = venuesResponse.map(venue => venue.venue); // Limpamos o resultado pra termos um array com os objetos de cada lugar apenas
        console.log(venuesArray);

        this.setState({ venues: venuesArray, axiosError: false }); // Salvamos isso no estado
      })
      .catch(error => {
        // Se der erroo, logamos e atualizamos o estado
        console.log(error);
        this.setState({ venues: [], axiosError: true });
      });
  }

  render() {
    return (
      <div className="App">
        <div className="test">
          {this.state.axiosError ? (
            <p>Oops, ocorreu um erro no Axios!</p>
          ) : (
            <ul>
              {this.state.venues.map(venue => (
                <li key={venue.id}>{venue.name}</li>
              ))}
            </ul>
          )}
        </div>
      </div>
    );
  }
}

export default App;
